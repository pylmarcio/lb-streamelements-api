# SE TIPS

LioranBoard extension for StreamElements not originally covered in LioranBoard.

The extension comes with premade buttons to make it easier to understand how it works.

SETIPS INIT - fill out all the values and refresh your transmitter (or close and reopen LB). The button will be automatically initiated every time you connect to Transmitter.
You can retrieve your JWT Token at https://streamelements.com/dashboard/account/channels. You can enable/disable trigger you wish to receive.

Donation - triggers with any donations coming through StreamElements.
Trigger: SETIPSTrigger

How to install an extension:

- Download the .lbe extension file
- Click on Install Extension in your LioranBoard Receiver
- Select the extension file you downloaded
- Select your default Transmitter you are using. Make 100% sure it is the correct one.
- Refresh your Transmitter or close and reopen Lioranboard Receiver.
- Most extensions include a premade deck with buttons. If you do not see one, create a new button, add "Send to Extensions" command and select the extension you just installed. If you can only see the extension name with no input fields, it means it was not installed correctly. Repeat steps above.


Inspired by https://github.com/christinna9031/LB-Streamlabs-Alerts

https://many.link/pylmarcio
